Summary: A GNOME daemon for automatically mounting/playing CDs.
Name: magicdev
Version: @VERSION@
Release: 1
Copyright: GPL
Group: Applications/System
Source: magicdev-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-root
BuildPrereq: libgnomeui-devel, libglade2-devel
Requires: gnome-mime-data

%description
Magicdev is a daemon that runs within the GNOME environment and
detects when a CD is removed or inserted. Magicdev handles running
autorun programs on the CD, updating the File Manager, and playing
audio CDs.

%prep
%setup -q

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT

export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%makeinstall
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

%find_lang %name

%clean
rm -rf $RPM_BUILD_ROOT

%post

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
SCHEMAS="magicdev.schemas"
for S in $SCHEMAS; do
  gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/$S > /dev/null
done

%files -f %{name}.lang
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/control-center-2.0/capplets
%{_datadir}/magicdev
%{_sysconfdir}/gconf/schemas/*.schemas

%changelog
* Thu Aug 29 2002 Owen Taylor <otaylor@redhat.com>
- Version 1.1.3 (Startup race condition, autorun dialog not going away, 
  RH bugzilla 72977)

* Fri Aug 23 2002 Owen Taylor <otaylor@redhat.com>
- Version 1.1.2 (Add --unique to gnome-cd command line, RH bugzilla 39208)

* Wed Aug 21 2002 Owen Taylor <otaylor@redhat.com>
- Version 1.1.1 (GUI tweaks, RH bugzilla 70973; put in right place in menus)

* Fri Jun  7 2002 Owen Taylor <otaylor@redhat.com>
- GNOME2 version

* Thu Aug 02 2001 Owen Taylor <otaylor@redhat.com>
- Merge spec file changes from Red Hat RPM
- Add BuildPrereq (http://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=44865)

* Tue Feb 27 2001 Trond Eivind Glomsrød <teg@redhat.com>
- langify
- use %%{_tmppath}

* Mon Jul 17 2000 Elliot Lee <sopwith@redhat.com>
- Add .mo files

* Fri Jul 14 2000 Matt Wilson <msw@redhat.com>
- defattr root

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri May 19 2000 Owen Taylor <otaylor@redhat.com>
- Rebuild for Winston

* Wed Mar  8 2000 Matt Wilson <msw@redhat.com>
- fix for autorun

* Thu Feb 10 2000 Preston Brown <pbrown@redhat.com>
- goad file is config file

* Thu Sep 09 1999 Elliot Lee <sopwith@redhat.com>
- Initial package
