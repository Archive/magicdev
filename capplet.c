/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/* Copyright (C) 1998 Redhat Software Inc.
 * Authors: Owen Taylor <otaylor@redhat.com>, Jonathan Blandford <jrb@redhat.com>
 */

#include "config.h"

#include <gnome.h>
#include <gdk/gdkx.h>
#include <gtk/gtkwindow.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>

/* Prototypes */
static void ok_callback (GtkDialog *dialog, int response_id, gpointer data);
static void write_config ();

GtkWidget *dialog = NULL;

GtkWidget *automount_cb;
GtkWidget *autorun_cb;
GtkWidget *autoplay_cd_cb, *autoplay_dvd_cb, *autoplay_cdr_cb;
GtkWidget *command_cd_entry, *command_dvd_entry, *command_cdr_entry;
GtkWidget *command_cd_fileentry, *command_dvd_fileentry, *command_cdr_fileentry;
GtkWidget *command_cd_hbox, *command_dvd_hbox, *command_cdr_hbox;
GConfClient *conf_client;

gboolean ignore_changes = FALSE;

typedef struct {
        gboolean automount;
        gboolean autorun;

	gboolean autoplay_cd;
        gchar *cd_command;
	gboolean autoplay_dvd;
	gchar *dvd_command;
	gboolean autoplay_cdr;
	gchar *cd_burn_command;
} Config;

Config config;

static void
set_sensitivity ()
{
        gtk_widget_set_sensitive (autorun_cb, config.automount);
        gtk_widget_set_sensitive (command_cd_hbox, config.autoplay_cd);
	gtk_widget_set_sensitive (command_dvd_hbox, config.autoplay_dvd);
	gtk_widget_set_sensitive (command_cdr_hbox, config.autoplay_cdr);
}

static void
update_config ()
{
        config.automount = GTK_TOGGLE_BUTTON (automount_cb)->active;
        config.autorun = GTK_TOGGLE_BUTTON (autorun_cb)->active;
        config.autoplay_cd = GTK_TOGGLE_BUTTON (autoplay_cd_cb)->active;
	config.autoplay_dvd = GTK_TOGGLE_BUTTON (autoplay_dvd_cb)->active;
        config.autoplay_cdr = GTK_TOGGLE_BUTTON (autoplay_cdr_cb)->active;
        g_free (config.cd_command);
        config.cd_command = gtk_editable_get_chars
		(GTK_EDITABLE (command_cd_entry), 0, -1);
	g_free (config.dvd_command);
	config.dvd_command = gtk_editable_get_chars
		(GTK_EDITABLE (command_dvd_entry), 0, -1);
        g_free (config.cd_burn_command);
	config.cd_burn_command = gtk_editable_get_chars
		(GTK_EDITABLE (command_cdr_entry), 0, -1);
}

static void
changed ()
{
        update_config();
        set_sensitivity ();
	if (ignore_changes == FALSE)
		write_config ();
}

#define SELECTION_NAME "_MAGICDEV_SELECTION"

static gboolean
magicdev_running (void)
{
        Atom clipboard_atom = gdk_x11_get_xatom_by_name (SELECTION_NAME);

        return XGetSelectionOwner (GDK_DISPLAY(), clipboard_atom) != None;
}

static void
check_magicdev_running (void)
{
        if ((config.automount || config.autorun ||
             config.autoplay_cd || config.autoplay_dvd || config.autoplay_cdr) &&
            !magicdev_running ()) {
                static const char * const argv[] = {
                        BINDIR "/magicdev",
                        NULL,
                };
                
                GError *error = NULL;

                g_spawn_async (g_get_home_dir(),
                               (char **)argv, NULL, 0,
                               NULL, NULL, NULL,
                               &error);

                if (error) {
                        GtkWidget *message = gtk_message_dialog_new (GTK_WINDOW (dialog),
                                                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                                                     GTK_MESSAGE_ERROR,
                                                                     GTK_BUTTONS_OK,
                                                                     "Error starting magicdev daemon: %s\n",
                                                                     error->message);

                        gtk_window_set_resizable (GTK_WINDOW (message), FALSE);
                        gtk_widget_show (message);

                        g_signal_connect (message, "response",
                                          G_CALLBACK (gtk_widget_destroy), NULL);
                        
                        g_error_free (error);
                }
        }
}

static void
update_widgets ()
{
        ignore_changes = TRUE;

        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (automount_cb),
                                      config.automount);
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (autorun_cb),
                                      config.autorun);
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (autoplay_cd_cb),
                                      config.autoplay_cd);
        gtk_entry_set_text (GTK_ENTRY (command_cd_entry), config.cd_command);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (autoplay_dvd_cb),
			              config.autoplay_dvd);
	gtk_entry_set_text (GTK_ENTRY (command_dvd_entry), config.dvd_command);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (autoplay_cdr_cb),
			              config.autoplay_cdr);
	gtk_entry_set_text (GTK_ENTRY (command_cdr_entry), config.cd_burn_command);

        set_sensitivity ();

        check_magicdev_running ();

        ignore_changes = FALSE;
}

static void
write_config ()
{
        gconf_client_set_bool(conf_client, "/apps/magicdev/do_automount",
                              config.automount, NULL);
        gconf_client_set_bool(conf_client, "/apps/magicdev/do_autorun",
                              config.autorun, NULL);
        gconf_client_set_bool(conf_client, "/apps/magicdev/do_cd_play",
                              config.autoplay_cd, NULL);
        gconf_client_set_string(conf_client, "/apps/magicdev/cd_play_command",
                                config.cd_command, NULL);
        gconf_client_set_bool(conf_client, "/apps/magicdev/do_cd_burn",
                              config.autoplay_cdr, NULL);
        gconf_client_set_string(conf_client, "/apps/magicdev/cd_burn_command",
                                config.cd_burn_command, NULL);
	gconf_client_set_bool(conf_client, "/apps/magicdev/do_dvd_play",
                              config.autoplay_dvd, NULL);
	gconf_client_set_string(conf_client, "/apps/magicdev/dvd_play_command",
                                config.dvd_command, NULL);
}

static void
read_config ()
{
        config.automount = gconf_client_get_bool (conf_client,
				"/apps/magicdev/do_automount", NULL);
        config.autorun = gconf_client_get_bool (conf_client,
			"/apps/magicdev/do_autorun", NULL);
        config.autoplay_cd = gconf_client_get_bool(conf_client,
			"/apps/magicdev/do_cd_play", NULL);
        config.cd_command = gconf_client_get_string(conf_client,
			"/apps/magicdev/cd_play_command", NULL);
        config.autoplay_cdr = gconf_client_get_bool(conf_client,
			"/apps/magicdev/do_cd_burn", NULL);
        config.cd_burn_command = gconf_client_get_string(conf_client,
			"/apps/magicdev/cd_burn_command", NULL);
	config.autoplay_dvd = gconf_client_get_bool(conf_client,
			"/apps/magicdev/do_dvd_play", NULL);
	config.dvd_command = gconf_client_get_string(conf_client,
			"/apps/magicdev/dvd_play_command", NULL);
        update_widgets ();
}

static void
config_changed_cb (GConfClient *client, guint id, GConfEntry *entry,
		gpointer data)
{
	read_config();
}

static void
ok_callback (GtkDialog *dialog, int response_id, gpointer data)
{
	GError *error = NULL;

	switch (response_id) {
		case GTK_RESPONSE_HELP:
		gnome_help_display_desktop (NULL, "user-guide",
					    "wgoscustdesk.xml",
					    "goscustdesk-73", &error);
		if (error) {
			GtkWidget *msg_dialog;
			char *msg;

			msg = g_strdup_printf ("<b>%s</b>\n%s.", _("There was an error displaying the help."), error->message);
			msg_dialog = gtk_message_dialog_new (GTK_WINDOW(dialog),
                                                             GTK_DIALOG_DESTROY_WITH_PARENT,
							     GTK_MESSAGE_ERROR,
							     GTK_BUTTONS_CLOSE,
							     msg);
			g_free (msg);
			gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (msg_dialog)->label), TRUE);

			gtk_window_set_resizable (GTK_WINDOW (msg_dialog), FALSE);
			gtk_dialog_run (GTK_DIALOG (msg_dialog));
			gtk_widget_destroy (msg_dialog);
			g_error_free (error);
		}

		break;
	default:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		gtk_main_quit ();
		//gnome_entry_save_history (GNOME_ENTRY (command_fileentry));
		break;
	}
}


static void
set_icon (GtkImage       *image,
          GtkIconTheme *icon_theme,
          const char     *icon_name)
{
	GdkPixbuf *pixbuf;

	pixbuf = gtk_icon_theme_load_icon (icon_theme, icon_name, 48, 0, NULL);
        if (pixbuf != NULL) {
                gtk_image_set_from_pixbuf (image, pixbuf);
		gdk_pixbuf_unref (pixbuf);
        }
}

static void
init_cd_capplet ()
{
        GtkWidget *icon;
        GladeXML *xml;
        char *glade_file;
        GtkIconTheme *icon_theme;
	GdkPixbuf *pixbuf;

	conf_client = gconf_client_get_default ();
	gconf_client_add_dir (conf_client,
			"/apps/magicdev",
			GCONF_CLIENT_PRELOAD_ONELEVEL,
			NULL);
	gconf_client_notify_add (conf_client,
			"/apps/magicdev", config_changed_cb,
			NULL, NULL, NULL);

        glade_file = g_concat_dir_and_file (MAGICDEVDIR, "cd-capplet.glade");
        xml = glade_xml_new (glade_file, NULL, NULL);
        g_free (glade_file);

	dialog = glade_xml_get_widget (xml, "dialog1");

        icon_theme = gtk_icon_theme_get_default ();

        icon = glade_xml_get_widget (xml, "image1");
        set_icon (GTK_IMAGE (icon), icon_theme, "gnome-dev-cdrom");
	icon = glade_xml_get_widget (xml, "image2");
        set_icon (GTK_IMAGE (icon), icon_theme, "gnome-dev-cdrom-audio");
	icon = glade_xml_get_widget (xml, "image3");
        set_icon (GTK_IMAGE (icon), icon_theme,  "gnome-dev-dvd");
	icon = glade_xml_get_widget (xml, "image4");
        set_icon (GTK_IMAGE (icon), icon_theme, "gnome-dev-cdrom");

	pixbuf = gtk_icon_theme_load_icon
		(icon_theme, "gnome-dev-cdrom", 48, 0, NULL);
	if (pixbuf != NULL) {
		gtk_window_set_default_icon (pixbuf);
		gdk_pixbuf_unref (pixbuf);
	}

        /* Extract data entry widgets */

        automount_cb = glade_xml_get_widget (xml, "automount-cb");
        autorun_cb = glade_xml_get_widget (xml, "autorun-cb");

        autoplay_cd_cb = glade_xml_get_widget (xml, "autoplay-cd-cb");
        command_cd_entry = glade_xml_get_widget (xml, "command-cd-entry");
        command_cd_fileentry = gnome_file_entry_gnome_entry (GNOME_FILE_ENTRY (glade_xml_get_widget (xml, "command-cd-fileentry")));
        command_cd_hbox = glade_xml_get_widget (xml, "command-cd-hbox");

        gnome_entry_set_history_id (GNOME_ENTRY (command_cd_fileentry), "CD_CAPPLET_ID");
//        gnome_entry_load_history (GNOME_ENTRY (command_cd_fileentry));
        gtk_combo_set_case_sensitive (GTK_COMBO (command_cd_fileentry), FALSE);
        //gnome_entry_prepend_history (GNOME_ENTRY (command_cd_fileentry), FALSE, DEFAULT_CD_COMMAND);

	autoplay_dvd_cb = glade_xml_get_widget (xml, "autoplay-dvd-cb");
	command_dvd_entry = glade_xml_get_widget (xml, "command-dvd-entry");
	command_dvd_fileentry = gnome_file_entry_gnome_entry (GNOME_FILE_ENTRY (glade_xml_get_widget (xml, "command-dvd-fileentry")));
	command_dvd_hbox = glade_xml_get_widget (xml, "command-dvd-hbox");
	gnome_entry_set_history_id (GNOME_ENTRY (command_dvd_fileentry), "CD_CAPPLET_ID");
//	gnome_entry_load_history (GNOME_ENTRY (command_dvd_fileentry));
	gtk_combo_set_case_sensitive (GTK_COMBO (command_dvd_fileentry), FALSE);
//	gnome_entry_prepend_history (GNOME_ENTRY (command_dvd_fileentry), FALSE, DEFAULT_CD_COMMAND);

        autoplay_cdr_cb = glade_xml_get_widget (xml, "autoplay-cdr-cb");
	command_cdr_entry = glade_xml_get_widget (xml, "command-cdr-entry");
	command_cdr_fileentry = gnome_file_entry_gnome_entry (GNOME_FILE_ENTRY (glade_xml_get_widget (xml, "command-cdr-fileentry")));
	command_cdr_hbox = glade_xml_get_widget (xml, "command-cdr-hbox");
	gnome_entry_set_history_id (GNOME_ENTRY (command_cdr_fileentry), "CD_CAPPLET_ID");
//	gnome_entry_load_history (GNOME_ENTRY (command_dvd_fileentry));
	gtk_combo_set_case_sensitive (GTK_COMBO (command_cdr_fileentry), FALSE);
//	gnome_entry_prepend_history (GNOME_ENTRY (command_dvd_fileentry), FALSE, DEFAULT_CD_COMMAND);

        read_config();
	update_widgets();

        gtk_signal_connect (GTK_OBJECT (automount_cb), "toggled",
                            GTK_SIGNAL_FUNC (changed), NULL);
        gtk_signal_connect (GTK_OBJECT (autorun_cb), "toggled",
                            GTK_SIGNAL_FUNC (changed), NULL);

        gtk_signal_connect (GTK_OBJECT (autoplay_cd_cb), "toggled",
                            GTK_SIGNAL_FUNC (changed), NULL);
        gtk_signal_connect (GTK_OBJECT (command_cd_entry), "changed",
                            GTK_SIGNAL_FUNC (changed), NULL);
	gtk_signal_connect (GTK_OBJECT (autoplay_dvd_cb), "toggled",
			    GTK_SIGNAL_FUNC (changed), NULL);
	gtk_signal_connect (GTK_OBJECT (command_dvd_entry), "changed",
			    GTK_SIGNAL_FUNC (changed), NULL);
			    
        gtk_signal_connect (GTK_OBJECT (autoplay_cdr_cb), "toggled",
			    GTK_SIGNAL_FUNC (changed), NULL);
	gtk_signal_connect (GTK_OBJECT (command_cdr_entry), "changed",
			    GTK_SIGNAL_FUNC (changed), NULL);

        gtk_widget_show (dialog);
        gtk_signal_connect(GTK_OBJECT (dialog), "response",
                           GTK_SIGNAL_FUNC(ok_callback), NULL);

        check_magicdev_running ();
}

int
main (int argc, char **argv)
{
        bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(PACKAGE, "UTF-8");
        textdomain (PACKAGE);

	gnome_program_init ("gnome-cd-properties", VERSION,
			LIBGNOMEUI_MODULE,
			argc, argv,
			GNOME_PARAM_APP_DATADIR, DATADIR, NULL);
        glade_gnome_init();

	init_cd_capplet ();
	gtk_main ();

        return 0;
}
