2004-07-30  Frederic Crozat  <fcrozat@mandrakesoft.com>

	* daemon.c: (app_check_devices), (app_device_activate_cdrom):
	Only mount devices at startup, don't start cd player and
	other actions.

2004-07-22  Bastien Nocera  <hadess@hadess.net>

	* configure.in: version 1.1.7

2004-07-22  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (app_close), (app_device_iso9660_add),
	(app_device_cdrom_set_state), (app_device_activate_cdrom),
	(app_device_check_change), (app_devices_update_is_mounted),
	(app_device_run_command), (app_device_mount), (app_device_autorun),
	(app_device_ask_mixed):
	Use g_spawn_* instead of gnome_execute_*, if a device is busy on
	startup, assume that it's because it's an already mounted CD drive,
	Fix the INACTIVE->ACTIVE state change (caused by the use of O_EXCL)
	(fixes autorun, Closes: #136062), Close the device before trying to
	mount it, or it will fail every time, Remove unused code and variables

2004-07-15  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (app_check_devices): mount all the CD devices on startup
	(Closes: #73543)

2004-07-15  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (mntent_has_fs_type), (app_device_run_command),
	(app_device_mount): listen to changes on "auto" types
	of filesystems as well (Closes: #104292)

2004-07-14  Bastien Nocera  <hadess@hadess.net>

	* capplet.c: (set_icon), (init_cd_capplet): use GtkIconTheme instead
	of GnomeIconTheme, fix possible crashes when the stock icons aren't
	available (Closes: #146450)

2004-05-01  Adam Weinberger  <adamw@gnome.org>

	* configure.in: Added en_CA to ALL_LINGUAS.

2004-04-09  Gareth Owen  <gowen72@yahoo.com>

	* configure.in: Added en_GB to ALL_LINGUAS

2004-04-07  Samúel Jón Gunnarsson  <sammi@techattack.nu>

	* configure.in: Added "is" to ALL_LINGUAS

2004-04-05  Pablo Saratxaga  <pablo@mandrakesoft.com>

	* configure.in: Added Macedonian (mk) to ALL_LINGUAS

2004-03-15  Bastien Nocera  <hadess@hadess.net>

	* capplet.c: (ok_callback), (init_cd_capplet):
	* cd-capplet.glade: add a help button, from a patch by Balamurali
	Viswanathan <balamurali.viswanathan@wipro.com> (Partially
	Closes: #136139), disabled for now as the help docs haven't been
	uploaded
	Fix a crash when the icon for the main window can't be found
	(Closes: #137228)

2004-03-04  Guntupalli Karunakar  <karunakar@freedomink.org>

	* configure.in: Added "pa" (Punjabi) to ALL_LINGUAS.

2004-02-27  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (app_device_floppy_add), (app_device_autorun):
	spit out some debug when in verbose mode (--verbose), and a device
	can't be monitored because of permissions (Closes: #101635)

2004-02-25  Bastien Nocera  <hadess@hadess.net>

	* === Released 1.1.6 ===

	* configure.in: updated

2004-02-24  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (app_device_iso9660_add): aww, fd leak if the
	CDROM_DRIVE_STATUS ioctl isn't supported, bad

2004-02-07  Robert Sedak  <robert.sedak@sk.htnet.hr>
 
         * configure.in: Added "hr" (Croatian) to ALL_LINGUAS.

2004-02-02  Kjartan Maraas  <kmaraas@gnome.org>

	* daemon.c: (app_device_add_aliases): s/g_dirname/g_path_get_dirname
	Closes bug #133172.

2004-01-07  Bastien Nocera  <hadess@hadess.net>

	* gnome-cd-properties.desktop.in: the label for the capplet is now
	"CD and DVD" without a name, the "Preferences" is implicit, just
	like for the other capplets

2004-01-07  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (app_device_cdrom_set_state): avoid autorun not working
	if the exclusive open failed (patch for the magicdev RPM from
	Owen Taylor)

2004-01-07  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (magicdev_verbose), (main),
	(app_device_is_blacklisted), (app_device_add), (app_device_remove),
	(app_device_add_aliases), (app_device_iso9660_add),
	(app_device_cdrom_set_state), (app_device_activate_cdrom):
	merge blacklist patch from the magicdev RPM, use exclusive open
	with O_EXCL for CD-Rom devices

2004-01-07  Bastien Nocera  <hadess@hadess.net>

	* === Released 1.1.5 ===

	* configure.in: updated

2003-12-22	Mohammad DAMT  <mdamt@bisnisweb.com>

	* configure.in: Added "id" in ALL_LINGUAS
	* po/id.po: Added Indonesian translation by Michel Alexandre Salim <salimma@users.sourceforge.net>

2003-12-15  Bastien Nocera  <hadess@hadess.net>

	* magicdev.schemas: removed the "Open file manager window" preference
	in the schemas file

2003-12-15  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (load_config): only have autorun if we have automount
	as well

2003-12-15  Bastien Nocera  <hadess@hadess.net>

	* .cvsignore: upd
	* daemon.c: (is_video_dvd), (app_device_activate_cdrom),
	(app_device_mount), (run_me), (app_device_autorun):
	make magicdev automount data DVDs, and double-check if a DVD is
	a video one before launching the application

2003-12-15  Bastien Nocera  <hadess@hadess.net>

	* cd-capplet.glade: change some labels (partial fix for #121786)

2003-12-14  Bastien Nocera  <hadess@hadess.net>

	* capplet.c: (set_sensitivity), (update_config), (update_widgets),
	(write_config), (read_config), (init_cd_capplet), (main):
	* cd-capplet.glade: remove "Open file manager window for
	newly mounted CD" option, as Nautilus doesn't handle it anymore, fix
	run-time warning setting the default icon
	(Closes: #116532)

2003-12-14  Bastien Nocera  <hadess@hadess.net>

	* gnome-cd-properties.desktop.in: Merge patch from Luca Ferretti
	<elle.uca@libero.it> to HIG-ify the desktop entry (Closes: #104291)

2003-12-14  Bastien Nocera  <hadess@hadess.net>

	* cd-capplet.glade: HIG update from Dennis Cranston
	<dennis_cranston@yahoo.com> and Luca Ferretti <elle.uca@libero.it>
	(Closes: #104289)

2003-12-03  Sanlig Badral  <badral@openmn.org>

	* configure.in: Added "mn" to ALL_LINGUAS.

2003-09-11  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (is_dvd): DVD_READ_STRUCT will actually fail if the media
	in the drive is not a DVD (Closes: #121351)

2003-09-06  Taneem Ahmed  <taneem@bengalinux.org>

	* configure.in: Added "bn" to ALL_LINGUAS.

2003-07-22  Hasbullah Bin Pit  <sebol@ikhlas.com>
                                                                                
	* configure.in: Added "ms" to ALL_LINGUAS.

2003-07-02  Bastien Nocera  <hadess@hadess.net>

	* .cvsignore: upd
	* configure.in: bring down the necessary libgnomeui requirement

2003-06-13  Laurent Dhima  <laurenti@alblinux.net>

	* configure.in: Added "sq" to ALL_LINGUAS.

2003-06-13  Jordi Mallach  <jordi@sindominio.net>

	* configure.in (ALL_LINGUAS): Added "ca" (Catalan).

2003-05-16  Danilo Šegan  <dsegan@gmx.net>

	* configure.in: Added "sr" and "sr@Latn" to ALL_LINGUAS.

2003-04-13  Duarte Loreto <happyguy_pt@hotmail.com>

        * configure.in: Added "pt" (Portuguese) to ALL_LINGUAS.

2003-02-20  Dmitry G. Mastrukov  <dmitry@taurussoft.org>

	* configure.in: Added Belarusian to ALL_LINGUAS.

2003-02-06  Daniel Yacob  <locales@geez.org>

	* configure.ac: Added "am" (Amharic) to ALL_LINGUAS.

Mon Jan 13 15:29:33 2003  Owen Taylor  <otaylor@redhat.com>

	* === Released 1.1.4 ===

	* gnome-cd-properties.desktop.in (Icon): Use a themed icon.

	* magicdev.spec.in: Update for new location of .desktop file.

Fri Jan 10 09:26:10 2003  Owen Taylor  <otaylor@redhat.com>

	* Fix typo in string. (Timo Meinen)

Thu Jan  9 21:51:04 2003  Owen Taylor  <otaylor@redhat.com>

	* cd-capplet.c daemon.c cd-capplet.glade: Add
	a patch from John Olby to add a command to 
	launch on blank recordable media. (#88476,
	Red Hat bugzilla #72514)

	* capplet.c (init_cd_capplet): Switch to using 
	gnome_icon_theme_*.

	* configure.in: Require libgnomeui-2.1.90

Thu Jan  9 18:13:19 2003  Owen Taylor  <otaylor@redhat.com>

	* daemon.c (app_device_ask_mixed): Ask what to do
	for CD's with both audio and data. (#97379,
	bugzilla.redhat.com #72516, Peter Winnberg)

Thu Jan  9 17:38:54 2003  Owen Taylor  <otaylor@redhat.com>

	* daemon.c (app_freeze): Add a facility for suspend
	the check timeout.

	* daemon.c (app_device_autorun): Use freeze/thaw.

Thu Jan  9 15:40:51 2003  Owen Taylor  <otaylor@redhat.com>

	* configure.in: Up version to 1.1.4.

	* daemon.c (mntent_has_fs_type): Account for the
	fact that the fstype entry might have multiple comma separated
	filesystem types in it. (Red Hat bugzilla, #78502)

	* daemon.c (app_check_fstab_changes): Accept the "users"
	option as well as the "user" option. (#101633, Samuel Stringham)

	* Makefile.am (Cappletdir): Install .desktop file into
	$(datadir)/control-center-2.0/capplets. (#95314, Ali Akcaagac)
	
2002-11-18  Fernando Herrera <fherrera@onirica.com>

	Reviewed by Bastien Nocera <hadess@hadess.net>

	* gnome-cd-properties.desktop.in: add X-GNOME-BUGZILLA stuff

2002-10-01  Stanislav Brabec  <sbrabec@suse.cz>

	* configure.in: Added cs to ALL_LINGUAS.

2002-09-26  Bastien Nocera  <hadess@hadess.net>

	* capplet.c: (main):
	* daemon.c: (main): i18n initialisation fixes by
	teuf@users.sourceforge.net (Christophe Fergeau) and myself (Closes:
	#87643)

Thu Aug 29 15:13:39 2002  Owen Taylor  <otaylor@redhat.com>

	* configure.in: Version 1.1.3

	* daemon.c (magicdev_get_lock): Fix bug that could
	result in multiple magicdevs being started at once.

	* daemon.c (app_device_autorun): Destory the autorun
	dialog when we are done with it.

Fri Aug 23 23:06:37 2002  Owen Taylor  <otaylor@redhat.com>

	* configure.in: Version 1.1.2

	* magicdev.schemas: Add --unique to the default 
	gnome-cd commandline.

	* daemon.c (app_device_autorun): Use a GtkDialog, not
	a GnomeDialog.

Fri Aug 23 16:59:23 2002  Jonathan Blandford  <jrb@redhat.com>

	* Makefile.am: change cd.desktop to be gnome-cd-properties.desktop
	to avoid conflicting with every single CD app out there.

Fri Aug 23 15:44:53 2002  Owen Taylor  <otaylor@redhat.com>

	* Makefile.am (Cappletdir): Move desktop file to
	$(datadir)/applications.

	* cd.desktop.in: Fix some validation problems.

Wed Aug 21 14:47:39 2002  Owen Taylor  <otaylor@redhat.com>

	* Version 1.1.1

	* cd-capplet.glade: Make toplevel not initially
	visible to prevent size change on startup.
	Fix some padding to be consistent.

	* cd.desktop.in (Categories): Add X-Red-Hat-Base; to
	Categories. And OnlyShowIn=GNOME.

Tue Aug 13 22:07:55 2002  Owen Taylor  <otaylor@redhat.com>

	* daemon.c: Fix bug where if the media-changed
	ioctl was present, mounting/unmounting wasn't
	detect. Also, check mount status on startup.

	* magicdev.schemas: Default to gtcd, not gnome-cd.
	(RH buzilla #70950)

	* cd-capplet.glade: Remove space in front of 
	Mount CD When inserted; remove expand from icons. 
	(RH bugzilla #70973)

Fri Jun  7 15:27:37 2002  Owen Taylor  <otaylor@redhat.com>

	* Version 1.1.0

	* daemon.c capplet.c: Use a selection rather than a
	file on disk for locking.

	* daemon.c capplet.c: Stop the magicdev program when
	it is completely turned off, restore it when it
	is turned back on; some people are very antsy
	about seeing magicdev run.

=== Merge in parts of Bastien Nocera's gnome-2 branch ===
	
2002-05-23  Bastien Nocera  <hadess@hadess.net>

	* cd-capplet.glade: some UI review by Calum, more stuff needs
	to be done that he told me

2002-05-22  Bastien Nocera  <hadess@hadess.net>

	* daemon.c: (main): changed gnome_init to new style

2002-05-20  Bastien Nocera  <hadess@hadess.net>

	* .cvsignore, Makefile.am, README, autogen.sh, capplet.c,
	cd-capplet.glade, cd.desktop.in, configure.in, daemon.c,
	magicdev.schemas: modified and added for gnome2 port
		- use gconf
		- dump goad
		- use intltool
		- make use of the CDROM_MEDIA_CHANGED ioctl if the drive
		  supports it.
		- detect insertiong of video DVDs
	* cd-capplet-strings.c, cd.desktop, magicdev.goad, magicdev.idl:
	removed

=== end merge of gnome-2 changes ===

2002-05-31  Pablo Saratxaga  <pablo@mandrakesoft.com>

	* configure.in: Added Vietnamese (vi) and Walloon (wa) to ALL_LINGUAS

2002-01-01  Christian Meyer  <chrisime@gnome.org>

	* cd.desktop: Added German strings.

Tue Sep 11 11:05:05 2001  Owen Taylor  <otaylor@redhat.com>

	* daemon.c (app_check_fstab_changes): Handle failure
	of setmntent (because /etc/fstab isn't readable) 
	gracefully instead of segfaulting.

	(Red Hat bugzilla 53485, GNOME bugzilla 58223)

Tue Sep 11 11:01:32 2001  Owen Taylor  <otaylor@redhat.com>

	* Makefile.am magicmedia.c daemon.c: Rename magicmedia.c
	file to refer left-over "magicmedia" name.

2001-09-08  Wang Jian  <lark@linux.net.cn>

	* configure.in(ALL_LINGUAS): Rename zh_CN.GB2312 to zh_CN

2001-08-31  Abel Cheung  <maddog@linux.org.hk>

	* configure.in (ALL_LINGUAS): zh_TW.Big5 -> zh_TW

Thu Aug  2 14:06:35 2001  Owen Taylor  <otaylor@redhat.com>

	* configure.in: Version 0.3.6

	* magicmedia.c (app_device_check_change): Fix problems
	with #endif.

	* magicdev.spec.in: Improvements from Red Hat RPM

2001-07-16  Kjartan Maraas  <kmaraas@gnome.org>

	* configure.in: Added "nn" to ALL_LINGUAS.
	
Fri Apr 13 16:59:51 2001  Owen Taylor  <otaylor@redhat.com>

	* magicmedia.c (app_devices_update_is_mounted): Check mtime/ctime
	before opening mtab to avoid atime updates.

Fri Apr 13 16:40:51 2001  Owen Taylor  <otaylor@redhat.com>

	* magicmedia.c: Comprehensive reindentation to GNOME style.

Tue Feb 27 15:53:30 2001  Owen Taylor  <otaylor@redhat.com>

	* configure.in: Version 0.3.5

	* Check for changes to the set of user/owner mountable
	devices in /etc/fstab, and on changes notify desktop.

	* magicmedia.c (app_run): Start and even if the list of
	mountable devices is empty, since things could be added
	later.

	* configure.in: Add -Wall to CFLAGS.

Wed Feb  7 17:08:47 2001  Owen Taylor  <otaylor@redhat.com>

	* magicmedia.c (cdrom_ioctl_frenzy): Turn off most
	of the ioctl setting. In particular leave the door locking
	at the default setting.

	* configure.in: Up version to 0.3.0

	* configure.in (ALL_LINGUAS): Add "pt_BR"

2000-12-24  Marius Andreiana  <mandreiana@yahoo.com>

	* configure.in: Added ro (Romanian) to ALL_LINGUAS.
	* cd.desktop: Added Romanian entries

2000-12-13  Stanislav Visnovsky  <visnovsky@nenya.ms.mff.cuni.cz>

	* configure.in: Added "sk" to ALL_LINGUAS.
	* cd.desktop: Added Slovak entries

2000-10-19  Yukihiro Nakai  <nakai@gnome.gr.jp>

	* configure.in: Add "zh_CN.GB2312" to ALL_LINGUAS.

2000-10-16  Jarkko Ranta  <jjranta@cc.joensuu.fi>

	* configure.in: Added "fi" to ALL_LINGUAS.
	* cd.desktop: Added Finnish entries

2000-10-05  Yukihiro Nakai  <nakai@gnome.gr.jp>

	* configure.in: Add "ja" to ALL_LINGUAS.
	* cd.desktop: Add Japanese translation.

2000-08-02  Fatih Demir	<kabalak@gmx.net>

	* configure.in: Added "tr" to ALL_LINGUAS.
	
	* magicdev.goad: Added the Turkish desription.

2000-07-14  Matt Wilson  <msw@redhat.com>

	* configure.in (ALL_LINGUAS): added nl

2000-05-25  Szabolcs BAN <shooby@gnome.hu>

	* po/hu.po: added

2000-04-10  Valek Filippov <frob@df.ru>

	* configure.in: Added "ru" to ALL_LINGUAS.

2000-03-08  Matt Wilson  <msw@redhat.com>

	* configure.in: version 0.2.7

	* magicmedia.c (app_device_autorun): change do_quit to !do_quit in
	for loop, otherwise we never run anything.  NULL terminate
	gnome_message_box_new() arguments.

2000-03-08  Birger Langkjer <birger.langkjer@image.dk>

	* configure.in: Added fr and pl to ALL_LINGUAS, updated danish da.po

2000-02-08 Elliot Lee <sopwith@redhat.com>
	* configure.in: Commit my uncommitted updates from 0.2.6,
	plus look for autorun.sh.

2000-01-23 Elliot Lee <sopwith@redhat.com>
	* configure.in (ALL_LINGUAS): Added Greek (el)

1999-12-30  Jesus Bravo Alvarez  <jba@pobox.com>

	* configure.in (ALL_LINGUAS): Added Galician (gl) and Spanish (es)

	* cd.desktop: Idem

1999-12-29  Richard Hult  <rhult@hem.passagen.se>

	* configure.in (ALL_LINGUAS): Add Swedish (sv) translation. 

	* cd.desktop: Dito.

1999-12-27  Yuri Syrota  <rasta@renome.rovno.ua>

	* configure.in: Added "uk" to ALL_LINGUAS.

1999-09-27  Tristan Tarrant  <ttarrant@etnoteam.it>

	* configure.in: Added "it" to ALL_LINGUAS.

Fri Sep 24 15:26:59 1999  Owen Taylor  <otaylor@redhat.com>

	* configure.in: Remove duplicate AM_CONFIG_HEADER
	line.

1999-09-24  Matthias Warkus  <mawa@iname.com>

	* configure.in: Added "de" to ALL_LINGUAS.

1999-09-23  Kjartan Maraas  <kmaraas@online.no>

	* configure.in: Added "da" and "no" to ALL_LINGUAS.
		
1999-09-22  Jonathan Blandford  <jrb@redhat.com>

	* magicmedia.c: patch from Elliot Lee to unmount drive (if
	possible) upon exitting.

Tue Sep 21 00:06:01 1999  Owen Taylor  <otaylor@redhat.com>

	* magicmedia.c: Up version to 0.2.3

	* magicmedia.c (app_device_autorun): Never start a new
	filemanager - if the user doesn't want a file manager,
	they don't want a file manager.

	* magicmedia.c (app_device_autorun): Don't free
	junk in case where create_window fails with an exception.

1999-09-20  Jonathan Blandford  <jrb@redhat.com>

	* capplet.c (init_cd_capplet): add history.  Broken.  In
	gnome-libs. Not to be fixed here, tonight. 

Mon Sep 20 16:55:58 1999  Owen Taylor  <otaylor@redhat.com>

	* Released version 0.2.2

	* Makefile.am (desktopdir): Put desktop enteries
	in Settings/Peripherals.

Fri Sep 17 16:52:59 1999  Owen Taylor  <otaylor@redhat.com>

	* Released version 0.2.0

	* configure.in Makefile.am: make it dist OK

Wed Sep 15 01:35:53 1999  Owen Taylor  <otaylor@redhat.com>

	* Makefile.am: Define GNOMELOCALEDIR

Wed Sep 15 01:29:30 1999  Owen Taylor  <otaylor@redhat.com>

	* */*: Automaked, gettextized, frobbed.

Tue Sep 14 17:05:41 1999  Owen Taylor  <otaylor@redhat.com>

	* magicmedia.c (app_device_autorun): When a device
	  is mounted, only create a new window if necessary,
	  and rescan all windows pointing to that directory.

	  magicmedia.c (app_desktop_refresh): Remove any
	  windows on the desktop pointing to unmounted
	  devices.

	* capplet.c (update_server_state): Make sure the
	  server is running or not running as appropriate
	  when we start up.

Mon Sep 13 17:18:24 1999  Owen Taylor  <otaylor@redhat.com>

	* magicmedia.c (app_device_mount): Reversed reversed
	  conditional.

	* Makefile: Install the .goad file.

	* capplet.c (write_config): Tell the running server
	  to reread the config file, activate a new one if
	  necessary.

Thu Sep  9 19:50:45 1999  Owen Taylor  <otaylor@redhat.com>

        * Makefile: Add install targets.
	
	* magicdev.c (main): Changed config keys to match
	  capplet.

	* cd-capplet.glade capplet.c: Added capplet for control.

